﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletReleaser : MonoBehaviour {
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Transform tr = transform;
        if ((tr.position.x > maxX || tr.position.x < minX) || (tr.position.y > maxY || tr.position.y < minY))
            BulletFactory.Instance.Release(this.gameObject);
    }
}
