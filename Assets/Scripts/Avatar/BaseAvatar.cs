﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAvatar : MonoBehaviour {
    public float health;


    [SerializeField]
    private float maxHealth;

    [SerializeField]
    private float maxSpeed;

    public float MaxSpeed
    {
        get
        {
            return this.maxSpeed;
        }
        set
        {
            this.maxSpeed = value;
        }
    }
    public float MaxHealth
    {
        get
        {
            return this.maxHealth;
        }
        set
        {
            this.maxHealth = value;
        }
    }

    public void Init()
    {
        health = maxHealth;
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void TakeDamage(float damage)
    {
        float newHealth = health - damage;
        if (newHealth <= 0)
            Die();
        health = newHealth;
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
