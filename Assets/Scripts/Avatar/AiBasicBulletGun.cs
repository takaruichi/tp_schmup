﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiBasicBulletGun : MonoBehaviour {
    public GameObject bullet;
    public float cooldown;
    public Vector3 offset;

    private float nextFire;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }

    public void Shoot()
    {
        if (Time.time >= nextFire)
        {
            nextFire = Time.time + cooldown;
            Instantiate(bullet, transform.position + offset, transform.rotation);
        }
    }
}
