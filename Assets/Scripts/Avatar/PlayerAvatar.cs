﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAvatar : BaseAvatar {
    [SerializeField]
    private float energy;
    public float Energy
    {
        get
        {
            return energy;
        }
        set
        {
            energy = value;
        }
    }

    public float noShootWait;

    [SerializeField]
    private float nextNoShootWait;
    public float NextNoShootWait
    {
        get
        {
            return nextNoShootWait;
        }
        set
        {
            nextNoShootWait = value;
        }
    }

    public float maxEnergy;
    public float energyRechargeCooldown;

    private float nextEnergyRecharge;
    [SerializeField]
    private bool outOfEnergy;
    public bool OutOfEnergy
    {
        get
        {
            return outOfEnergy;
        }
        set
        {
            outOfEnergy = value;
        }
    }

    private void Start()
    {
        Init();
        MaxSpeed = 5;
        energy = 5;
    }

    // Update is called once per frame
    void Update () {
        /*if (energy <= 0)
        {
            noEnergy = true;
        }

        if (noEnergy && Time.time >= nextEnergyRecharge)
        {
            nextEnergyRecharge = Time.time + (energyRechargeCooldown / 2.0f);
            energy += 0.5f;
        }

		if (Time.time >= nextEnergyRecharge && Time.time >= noShootWait)
        {
            nextEnergyRecharge = Time.time + energyRechargeCooldown;
            ++energy;
        }*/
	}
}
