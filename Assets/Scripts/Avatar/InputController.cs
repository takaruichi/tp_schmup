﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {
    private Engine engine;
    private BulletGun bulletgun;

    void Start()
    {
        engine = GetComponent<Engine>();
        bulletgun = GetComponent<BulletGun>();
    }

    // Update is called once per frame
    void Update () {
        //move
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 speed = new Vector3(horizontal, vertical, 0.0f);
        engine.speed = speed;

        //shoot
        if (Input.GetButton("Jump"))
        {
            bulletgun.Shoot();
        }
    }
}
