﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletGun : MonoBehaviour {
    public GameObject bullet;
    public float cooldown;
    public Vector3 offset;

    private float nextFire;
    private PlayerAvatar pa;
    public bool isShooting;

    // Use this for initialization
    void Start () {
        pa = GetComponent<PlayerAvatar>();
	}
	
	// Update is called once per frame
	void Update () {
        if (isShooting)
        {
            pa.Energy -= Time.deltaTime;
            if (pa.Energy <= 0)
            {
                isShooting = false;
                pa.OutOfEnergy = true;
            }

        }
        else if (pa.Energy < pa.maxEnergy && !isShooting)
        {
            pa.Energy += Time.deltaTime;
        }
        if (pa.Energy >= pa.maxEnergy)
            pa.OutOfEnergy = false;
        if (isShooting && Time.time > nextFire)
        {
            isShooting = false;
        }
    }

    public void Shoot()
    {
        if (Time.time >= nextFire && pa.Energy > 0 && !pa.OutOfEnergy)
        {
            isShooting = true;
            nextFire = Time.time + cooldown;
            pa.NextNoShootWait = Time.time + pa.noShootWait;
            //Instantiate(bullet, transform.position + offset, transform.rotation);
            BulletFactory.Instance.GetBullet(BulletType.PlayerBullet, transform.position + offset);
        }
    }
}
