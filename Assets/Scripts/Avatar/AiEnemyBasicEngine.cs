﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiEnemyBasicEngine : MonoBehaviour {
    //private Vector3 speed;
    private Vector3 position;
    private BaseAvatar bAvatar;

    // Use this for initialization
    void Start()
    {
        bAvatar = GetComponent<BaseAvatar>();
    }

    // Update is called once per frame
    void Update()
    {
        move();
    }

    void move()
    {
        position = transform.position;
        Vector3 newposition = position + new Vector3(-0.5f,0.0f,0.0f) * bAvatar.MaxSpeed * Time.deltaTime;
        transform.position = newposition;
    }
}
