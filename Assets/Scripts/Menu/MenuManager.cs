﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    public Button startBp;
    public Button quitBp;

	// Use this for initialization
	void Start () {
        startBp.GetComponent<Button>().onClick.AddListener(StartButtonEvent);
        quitBp.GetComponent<Button>().onClick.AddListener(QuitButtonEvent);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void StartButtonEvent()
    {
        SceneManager.LoadScene("SampleScene");
    }

    void QuitButtonEvent()
    {
        Application.Quit();
    }
}
