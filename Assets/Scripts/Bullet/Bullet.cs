﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    [SerializeField]
    private float speed;

    public float Speed
    {
        get
        {
            return this.speed;
        }
        set
        {
            this.speed = value;
        }
    }
    [SerializeField]
    private float damage;
    public float Damage
    {
        get
        {
            return this.damage;
        }
        set
        {
            this.damage = value;
        }
    }
    private Vector2 position;
    public Vector2 Position {
        get {
            return transform.position;
        }
        set
        {
            transform.position = value;
        }
    }

    [SerializeField]
    private BulletType type;
    public BulletType Type
    {
        get
        {
            return type;
        }
        set
        {
            type = value;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdatePosition();
	}

    public virtual void UpdatePosition()
    {
        
    }
    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        other.GetComponent<BaseAvatar>().TakeDamage(damage);
        //Destroy(gameObject);
        BulletFactory.Instance.Release(gameObject);
    }
}
