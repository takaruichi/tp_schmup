﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BulletType { PlayerBullet, EnemyBullet };

public class BulletFactory : MonoBehaviour {
    public static BulletFactory Instance { get; private set; }
    private List<GameObject> availableBullet;
    private Dictionary<BulletType, List<GameObject>> bulletCache = new Dictionary<BulletType, List<GameObject>>();

    public GameObject playerBullet;
    public GameObject enemyBullet;

    private int baseNumberOfBullets = 10;

    private void Awake()
    {
        Debug.Assert(BulletFactory.Instance == null);
        BulletFactory.Instance = this;
        availableBullet = new List<GameObject>();

        List<GameObject> bullets = new List<GameObject>();
        for (int i = 0; i < this.baseNumberOfBullets; ++i)
        {
            bullets.Add(GameObject.Instantiate(this.playerBullet));
        }
        this.bulletCache.Add(BulletType.PlayerBullet, bullets);

        for (int i = 0; i < this.baseNumberOfBullets; ++i)
        {
            bullets.Add(GameObject.Instantiate(this.enemyBullet));
        }
        this.bulletCache.Add(BulletType.EnemyBullet, bullets);
    }

    public GameObject GetBullet(BulletType type, Vector2 spawnPosition)
    {
        List<GameObject> bulletList;
        if (!this.bulletCache.TryGetValue(type, out bulletList))
        {
            bulletList = new List<GameObject>();
            this.bulletCache.Add(type, bulletList);

        }
        GameObject b = null;
        if (bulletList.Count > 0)
        {
            b = bulletList[0];
            bulletList.RemoveAt(0);

        }
        else
        {
            switch (type)
            {
                case BulletType.PlayerBullet:
                    //b = Instantiate(playerBullet, transform.position, transform.rotation);
                    //b = GameObject.Instantiate(this.playerBullet);

                    break;
                case BulletType.EnemyBullet:
                    b = GameObject.Instantiate(this.enemyBullet);
                    break;
                //default:
                    //throw new Argu
            }
        }
        /**/
        b = GetAvailableBullet(type);

        Bullet bullet = b.GetComponent<Bullet>();
        bullet.Position = spawnPosition;
        bullet.gameObject.SetActive(true);

        return b;
    }

    private GameObject GetAvailableBullet(BulletType type)
    {
        if (availableBullet.Count == 0)
            return GameObject.Instantiate(GetPrefab(type));


        foreach (GameObject b in availableBullet)
        {
            if (b.GetComponent<Bullet>().Type.Equals(type))
            {
                b.GetComponent<Renderer>().enabled = true;
                availableBullet.Remove(b);
                return b;
            }
        }

        return GameObject.Instantiate(GetPrefab(type));
    }

    private GameObject GetPrefab(BulletType type)
    {
        switch (type) {
            case BulletType.PlayerBullet:
                return this.playerBullet;
            case BulletType.EnemyBullet:
                return this.enemyBullet;
        }
        return null;
    }

    public void Release(GameObject bullet)
    {
        /*Debug.Log("release");
        bullet.GetComponent<Renderer>().enabled = false;
        availableBullet.Add(bullet);*/
        BulletType type = bullet.GetComponent<Bullet>().Type;

        List<GameObject> bulletList;
        if (!this.bulletCache.TryGetValue(type, out bulletList))
        {
            bulletList = new List<GameObject>();
            this.bulletCache.Add(type, bulletList);

        }

        bulletList.Add(bullet.gameObject);
        bullet.gameObject.SetActive(false);
    }
}
