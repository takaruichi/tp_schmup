﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBullet : Bullet {
    private Bullet b;
    
    // Use this for initialization
    void Start () {
        b = GetComponent<Bullet>();
	}
	
	// Update is called once per frame
	void Update () {
        UpdatePosition();
	}

    public override void UpdatePosition()
    {
        Vector3 position = transform.position;
        Vector3 newposition = new Vector3(position.x + (b.Speed * Time.deltaTime), position.y, position.z);
        transform.position = newposition;
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
    }
}
