﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public float enemySpawnCooldown;
    public float enemySpawnX;
    public float enemyMinSpawnY;
    public float enemyMaxSpawnY;
    public GameObject player;
    public GameObject enemy;

    public static GameObject instantiatedPlayer;

    private float nextEnemySpawn;

	// Use this for initialization
	void Start () {
        instantiatedPlayer = Instantiate(player, new Vector2(-7.0f, 0.0f), new Quaternion());
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time >= nextEnemySpawn)
        {

            nextEnemySpawn = Time.time + enemySpawnCooldown;
            Instantiate(enemy, new Vector2(enemySpawnX, Random.Range(enemyMinSpawnY, enemyMaxSpawnY)), new Quaternion());
        }
	}
}
