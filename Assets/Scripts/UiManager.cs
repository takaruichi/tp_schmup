﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour {
    public Slider healthbar;
    public Slider energybar;

    public GameObject player;
    private PlayerAvatar pa;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        //energybar.value = pa.Energy;
        healthbar.value = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAvatar>().health;
        energybar.value = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAvatar>().Energy;
	}
}
